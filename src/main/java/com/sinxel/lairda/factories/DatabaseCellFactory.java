package com.sinxel.lairda.factories;

import com.sinxel.lairda.models.DatabaseItemView;
import com.sinxel.lairda.values.Colors;
import com.sinxel.lairda.values.ICIcons;
import com.sinxel.lairda.values.MdIcons;
import com.sinxel.lairda.widgets.IcoFont;
import com.sinxel.lairda.widgets.MdIcon;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeCell;

public class DatabaseCellFactory extends TreeCell<DatabaseItemView> {

    @Override
    protected void updateItem(DatabaseItemView item, boolean empty) {
        super.updateItem(item, empty);
        if(empty){
            setText(null);
            setGraphic(null);
        }else{
            String text = item.getName();
            setText(text);
            switch (item.getType()){
                case CONNECTION -> setGraphic(new IcoFont(ICIcons.CONNECTION));
                case DATABASE -> setGraphic(new IcoFont(ICIcons.DATABASE));
                case TABLE -> setGraphic(new IcoFont(ICIcons.TABLE));
                default -> setGraphic(new IcoFont(ICIcons.CLOSE));
            }

            setContextMenu(item.getMenu());
        }
    }




}
