package com.sinxel.lairda.factories;

import com.sinxel.lairda.models.ResultCell;

import com.sinxel.lairda.values.ICIcons;
import com.sinxel.lairda.widgets.IcoFont;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

public class DataTableCell extends TableCell<ObservableList<ResultCell>, String> {

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if(empty){
            setText(null);
            setContextMenu(null);
        }else{
            setContextMenu(createContextMenu(item));
            setText(item);
        }
        setGraphic(null);

    }

    ContextMenu createContextMenu(String value){
        ContextMenu mnuOptions = new ContextMenu();
        MenuItem mniCopyValue = new MenuItem("Copy Value");
        mnuOptions.getItems().add(mniCopyValue);
        mniCopyValue.setOnAction(ev->copyValue(value));
        return mnuOptions;
    }

    void copyValue(String value){
        final Clipboard clipboard = Clipboard.getSystemClipboard();
        final ClipboardContent content = new ClipboardContent();
        content.putString(value);

        clipboard.setContent(content);
    }
}
