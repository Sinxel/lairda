package com.sinxel.lairda.factories;

import com.sinxel.lairda.models.Cluster;
import com.sinxel.lairda.models.Datum;
import com.sinxel.lairda.widgets.IcoFont;
import javafx.scene.control.TreeCell;

import javax.xml.crypto.Data;

public class ClusterTreeCell extends TreeCell<Cluster> {
    public static final int ID_ADD_CLUSTER = -1;

    @Override
    protected void updateItem(Cluster item, boolean empty) {
        if(empty){
            setText(null);
            setGraphic(null);
        }else{
            String title = "["+item.getId()+"]";
            if(item.getData().containsKey("title")){
                title = item.getData().get("title").getValue();
            }
            String icon = null;
            if(item.getData().containsKey("icon")){
                icon = item.getData().get("icon").getValue();
            }
            setText(title);
            setGraphic(new IcoFont(icon));
        }
    }
}
