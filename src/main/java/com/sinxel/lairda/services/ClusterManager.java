package com.sinxel.lairda.services;

import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.models.AppNotification;
import com.sinxel.lairda.models.Cluster;
import com.sinxel.lairda.models.Datum;
import com.sinxel.lairda.models.DataMap;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClusterManager {
    public static String getClusterPath(int id){
        if(id<1) return "0/";
        String sql = "SELECT path FROM clusters WHERE id=? LIMIT 1"; //"0/"
        try {
            PreparedStatement stm = Lairda.getConnection().prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if(!rs.next()) return id+"/";
            String currentPath = rs.getString("path"); //"0/"
            if(currentPath.equals("")) return id+"/";
            if(currentPath.charAt(currentPath.length()-1)!='/') currentPath+='/';
            return currentPath+id+"/";
        } catch (SQLException ex) {
            ex.printStackTrace();

        }
        return ""; //should never happen

    }
    public static int insertCluster(int parentId, DataMap data, boolean isVisible){

        int id;
        String path = getClusterPath(parentId);
        try {
            PreparedStatement stm = Lairda.getConnection().prepareStatement("INSERT INTO clusters ( path, visible,createdAt) VALUES( ?, ?,  ?);");
            stm.setString(1, path);
            stm.setInt(2, isVisible?1:0);
            int epoch = Math.round(System.currentTimeMillis()/1000f);
            stm.setInt(3, epoch);
            stm.executeUpdate();
            ResultSet rs = stm.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
            stm.close();

            for (Datum record : data.values()) {
                String sql = "INSERT INTO data (cid, key, value, type) VALUES (?, ?, ?, ?);";
                stm = Lairda.getConnection().prepareStatement(sql);
                stm.setInt(1, id);
                stm.setString(2, record.getKey());
                stm.setString(3, record.getValue());
                stm.setString(4, record.getType().toString());
                stm.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
        return id;
    }

    public static Cluster getCluster(int id){
        Cluster cluster = new Cluster();
        if(id==0){
            return getRootCluster();
        }
        String sql = "SELECT * FROM clusters WHERE id=? LIMIT 1;";
        try{
            PreparedStatement stm = Lairda.getConnection().prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if(!result.next()) return null;
            cluster.setId(result.getInt("id"));
            cluster.setEpoch(result.getLong("createdAt"));
            cluster.setPath(result.getString("path"));
            stm.close();
            result.close();
            cluster.setChildren(getChildrenCount(cluster.getPath()+"/"+cluster.getId()));
            cluster.setData(getClusterData(cluster.getId()));

        }catch(Exception ex){
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Could not get cluster", "Error while trying to get cluster id: " + id));
        }
        return cluster;
    }
    public static ArrayList<Cluster> getAllClusters(int parentId){
        String path = getClusterPath(parentId);
        ArrayList<Cluster> clusters = new ArrayList<>();
        String sql = "SELECT id FROM clusters WHERE path=? AND visible=1 ORDER BY createdAt DESC";
        try{
            PreparedStatement stm = Lairda.getConnection().prepareStatement(sql);
            stm.setString(1, path);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                clusters.add(getCluster(rs.getInt("id")));
            }

        }catch (Exception ex){
            Lairda.getInstance().appendToConsole(ex.getMessage());
        }
        return clusters;
    }
    public static DataMap getClusterData(int id){
        DataMap data = new DataMap();
        String sql = "SELECT * FROM data WHERE cid=?";
        try {
            PreparedStatement stm = Lairda.getConnection().prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Datum datum = new Datum(Datum.parseType(rs.getString("type")), rs.getString("key"), rs.getString("value"));
                data.put(rs.getString("key"), datum);
            }

        } catch (SQLException e) {
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Cluster Data Error", "Could not fetch the data of the cluster: " + id));
        }
        return data;
    }
    public static int getChildrenCount(String path){
        String sql = "SELECT COUNT(id) FROM clusters WHERE path=?";
        try {
            PreparedStatement stm = Lairda.getConnection().prepareStatement(sql);
            stm.setString(1, path);
            ResultSet rs = stm.executeQuery();
            rs.next();
            return  rs.getInt(1);
        } catch (SQLException e) {
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Couldn't get children count" , "We could not determine the children count of the requested cluster"));
            return 0;
        }

    }

    public static  Cluster getRootCluster(){
        Cluster cluster = new Cluster();
        cluster.setId(0);
        cluster.setPath("0");
        cluster.setEpoch(-1);
        DataMap data = new DataMap();
        data.put("title", new Datum("title", "[ROOT]"));
        data.put("text", new Datum("text", "This is the top cluster"));
        cluster.setData(data);
        return cluster;
    }
    public static int insertCluster(int parentId, DataMap data){
        return insertCluster(parentId, data, true);
    }
}
