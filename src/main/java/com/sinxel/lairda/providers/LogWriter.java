package com.sinxel.lairda.providers;

import java.io.*;
import java.nio.file.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class LogWriter {
    private static LogWriter _instance;

    private LogWriter(){

    }
    public static LogWriter getInstance(){
        if(_instance==null) _instance= new LogWriter();
        return _instance;
    }

    public void error(String message){
        error(message, null);
    }

    public void warn(String message){
        log("Warning", message, null);
    }

    public void error(String message, Exception stackTrace){
        log("Error", message, stackTrace);
    }

    private void log(String type, String message, Exception stackTrace){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String log = "---------------------------------------------\n";
        log += "[" + type + "]\n";
        log += "On: " + formatter.format(LocalDateTime.now()) + "\n";
        log += "---------------------------------------------\n";
        log += message+"\n";
        log += "\n\n\n";
        if(stackTrace!=null){
            log+= "### STACKTRACE\n";
            StringWriter writer = new StringWriter();
            PrintWriter printer = new PrintWriter(writer);
            stackTrace.printStackTrace(printer);
            log += writer.toString();
            log += log += "\n";
            log += "---------------------------------------------\n";
        }

        try {
            String fileName = ConfigProvider.getInstance().getString("logFile", "data/lairda.log");
            if(!Files.exists(Paths.get(fileName))){
                Files.createFile(Paths.get(fileName));
            }
            Path logFile = Paths.get(fileName);
            Files.writeString(logFile, log, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
