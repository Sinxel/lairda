package com.sinxel.lairda.providers;

import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.models.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.json.JSONObject;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class MySQL implements DatabaseConnection{
    Connection connection;
    DataMap connectionData;
    public MySQL(DataMap connectionData){
        this.connectionData = connectionData;
        try{

            String host = connectionData.valueOf("host");
            String port = connectionData.valueOf("port");
            String user = connectionData.valueOf("user");
            String password  = connectionData.valueOf("password");
            String db = connectionData.valueOf("database");

            System.out.println(db);
            connection = DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/" + db, user, password);

        }catch(Exception ex){
            System.err.println("NO CONNECTION");
            System.err.println(ex.getMessage());
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Could not connect to MySQL[" + connectionData.get("database").getValue()+"]", ex.getMessage()));
        }
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public ArrayList<String> getDatabases() {
        ArrayList<String> databases = new ArrayList<>();
        try{
            PreparedStatement stm = connection.prepareStatement("SHOW DATABASES;");
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                databases.add(rs.getString("database"));
            }

        }catch (Exception ex){
            ex.printStackTrace();
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Could not list database", "Connection error\nCould not list databases."));
        }
        return databases;
    }

    @Override
    public ArrayList<String> getTables() {
        ArrayList<String> tables = new ArrayList<>();
        try {
            Statement stm = connection.createStatement();

            ResultSet rs = stm.executeQuery("SHOW TABLES;");
            while(rs.next()){
                tables.add(rs.getString(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return tables;
    }

    @Override
    public DataResult getDefaultTableView(String tableName) {
        return executeSql("SELECT * FROM " + tableName + " LIMIT 1000;");
    }
    private DataResult executeSelect(String sql){
        DataResult result = new DataResult();
        try {
            Statement stm = connection.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            ResultSetMetaData meta = rs.getMetaData();
            int columnCount = meta.getColumnCount();
            ArrayList<String>  columns = new ArrayList<>();
            columns.add("#");
            for(int i=1; i<=columnCount; i++){
                columns.add(meta.getColumnLabel(i));
            }
            result.setColumns(columns);
            ObservableList<ObservableList<ResultCell>> rows = FXCollections.observableArrayList();
            int[] counter = {0};
            while(rs.next()){
                counter[0]++;
                ObservableList<ResultCell> row = FXCollections.observableArrayList();

                columns.forEach((name)->{
                    ResultCell cell;
                    if(name.equals("#")){
                        cell = new ResultCell(name, String.valueOf(counter[0]));
                        row.add(cell);
                        return;
                    }
                    try {
                        Object ob = rs.getObject(name);

                        if(ob==null){
                            cell = new ResultCell(name,"[NULL]");
                        }else{
                            cell = new ResultCell(name, ob.toString());
                        }
                        row.add(cell);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
                rows.add(row);
            }
            result.setRows(rows);

        } catch (SQLException throwables) {
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "SQL Error", throwables.getMessage()));
        }
        return result;
    }
    private DataResult executeUpdate(String sql){
        DataResult result = new DataResult();
        try{
            Statement stm = connection.createStatement();
            int xres = stm.executeUpdate(sql);
            ArrayList<String> columns = new ArrayList<>();
            columns.add("Type");
            columns.add("Affected Rows");
            result.setColumns(columns);
            String[] tokens = sql.trim().split(" ");

            ObservableList<ObservableList<ResultCell>> rows = FXCollections.observableArrayList();
            ObservableList<ResultCell> row = FXCollections.observableArrayList();
            row.add(new ResultCell("Type", tokens[0].toUpperCase(Locale.ROOT)));
            row.add(new ResultCell("Affected Rows", String.valueOf(xres)));
            rows.add(row);
            result.setRows(rows);

        }catch (Exception ex){
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "SQL Error", ex.getMessage()));
        }
        return result;
    }
    @Override
    public DataResult executeSql(String sql) {
        sql = sql.trim();
        String[] tokens = sql.split(" ");
        if(tokens[0].toLowerCase(Locale.ROOT).equals("select")){
            return executeSelect(sql);
        }else{
            return executeUpdate(sql);
        }
    }

    public static DataMap getConnectionData() {
        DataMap map = new DataMap();
        Datum host = new Datum("host", "localhost");
        host.setExtra(new JSONObject("{isRequired: true}"));
        map.add(host);
        Datum port = new Datum(Datum.Type.Number, "port" , "3306");
        port.setExtra(new JSONObject("{isRequired: true}"));
        map.add(port);
        map.add(new Datum("user"));
        map.add(new Datum(Datum.Type.Secret, "password"));
        return map;

    }
}
