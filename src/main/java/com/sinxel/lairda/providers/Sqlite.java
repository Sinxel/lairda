package com.sinxel.lairda.providers;
import com.sinxel.lairda.Lairda;
import org.sqlite.SQLiteConfig;
import org.sqlite.mc.SQLiteMCWxAES256Config;

import java.sql.*;


public class Sqlite {
    private  static Connection connection;

    public Sqlite(String dataFile, String key){
        String connectionString = "jdbc:sqlite:file:"+dataFile+"?cipher=sqlcipher&key="+key+"&legacy=4";
        try {
            connection = DriverManager.getConnection(connectionString);
            Statement stm = connection.createStatement();
            ResultSet result = stm.executeQuery("SELECT value FROM settings WHERE name='version';");
            result.next();
            System.out.println(result.getString(1));

        } catch (Exception ex){
            connection = null;
            System.err.println(ex.getMessage());
        }
    }

    public static void changePassword(String dataFile, String oldKey, String key){
        String connectionString = "jdbc:sqlite:file:"+dataFile+"?cipher=sqlcipher&key="+oldKey+"&legacy=4&rekey="+key;
        try {
            connection = DriverManager.getConnection(connectionString);
            Statement stm = connection.createStatement();
            ResultSet result = stm.executeQuery("SELECT value FROM settings WHERE name='version';");
            result.next();
            System.out.println(result.getString(1));

        } catch (Exception ex){
            connection = null;
            System.err.println(ex.getMessage());
        }
    }
    public static void createDefaultDatabase(String fileName, String key){
        String connectionString = "jdbc:sqlite:file:"+fileName+"?cipher=sqlcipher&key="+key+"&legacy=4";
        try {
            Connection creator = DriverManager.getConnection(connectionString);
            createSettingsTable(creator);
            createDataTable(creator);
            createCacheTable(creator);
            createClustersTable(creator);

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }



    private static void createClustersTable(Connection conn){
        String sql = "CREATE TABLE clusters ("+
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "path VARCHAR(255) NOT NULL," +
                "visible INT DEFAULT 1," +
                "createdAt INT NOT NULL"+
                ");";
        try {
            Statement stm = conn.createStatement();
            stm.executeUpdate(sql);
            stm.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void createCacheTable(Connection conn){
        String sql = "CREATE TABLE cache ("+
                "url VARCHAR(255) UNIQUE," +
                "path VARCHAR(255) NOT NULL"+
                ");";
        try {
            Statement stm = conn.createStatement();
            stm.executeUpdate(sql);
            stm.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void createSettingsTable(Connection con){
        String sql = "CREATE TABLE settings ("+
                "name VARCHAR(200) NOT NULL," +
                "value TEXT NOT NULL"+
                ");";
        Statement stm;
        try {
            stm = con.createStatement();
            stm.executeUpdate(sql);
            sql = "INSERT INTO settings (name, value) VALUES('version', 0.1);";
            stm.close();
            stm = con.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
    private static void createDataTable(Connection conn){
        String sql = "CREATE TABLE data ("+
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "cid INT NOT NULL," +
                "key VARCHAR(255) NOT NULL," +
                "value TEXT NOT NULL," +
                "type VARCHAR(255) DEFAULT 'text'" +
                ");";
        try {
            Statement stm = conn.createStatement();
            stm.executeUpdate(sql);
            stm.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void setSetting(String key, String value){
        String currentValue = getSetting(key);
        String sql = "UPDATE settings SET value=? WHERE name=?;";
        if(currentValue==null)
            sql = "INSERT INTO settings (value, name) VALUES (?, ?);";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, value);
            stm.setString(2, key);
            stm.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public String getSetting(String name){
        String sql = "SELECT value FROM settings WHERE name=? LIMIT 1;";

        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, name);
            ResultSet rs = stm.executeQuery();
            if(!rs.next()) return null;
            return rs.getString("value");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;

    }

    public boolean isOpen(){
        return connection!=null;
    }

    public Connection getConnection(){
        return  connection;
    }


}
