package com.sinxel.lairda.providers;

import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.models.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.json.JSONObject;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class OracleDB implements DatabaseConnection{
    Connection connection;
    DataMap connectionData;
    public OracleDB(DataMap connectionData){
        this.connectionData = connectionData;
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@"+connectionData.get("host").getValue()+":"+connectionData.get("port").getValue()+":" + connectionData.get("SID").getValue(), connectionData.get("user").getValue(), connectionData.get("password").getValue());

        }catch(Exception ex){
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Could not connect to Oracle[" + connectionData.get("SID").getValue()+"]", ex.getMessage()));
        }
    }

    public static DataMap getConnectionData() {
        DataMap map = new DataMap();
        Datum host = new Datum("host");
        host.setExtra(new JSONObject("{isRequired: true}"));
        map.add(host);
        Datum port = new Datum(Datum.Type.Number, "port");
        port.setExtra(new JSONObject("{isRequired: true}"));
        map.add(port);
        map.add(new Datum("SID"));
        map.add(new Datum("user"));
        map.add(new Datum(Datum.Type.Secret, "password"));
        return map;

    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public ArrayList<String> getDatabases() {
        ArrayList<String> result = new ArrayList<>();
        result.add(connectionData.get("SID").getValue());
        return result;
    }

    @Override
    public ArrayList<String> getTables() {
        ArrayList<String> tables = new ArrayList<>();
        String sql = "SELECT table_name FROM all_tables WHERE owner='"+ connectionData.valueOf("user").toUpperCase(Locale.ROOT)+"'";
        try {
            Statement stm = connection.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                tables.add(rs.getString(1));
            }
        } catch (SQLException e) {
            Lairda.getInstance().appendToConsole(e.getMessage());
        }
        return tables;
    }

    @Override
    public DataResult getDefaultTableView(String tableName) {
        DataResult result = new DataResult();
        try {
            Statement stm = connection.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM " + tableName + " WHERE rownum<=1000");
            ResultSetMetaData meta = rs.getMetaData();
            int columnCount = meta.getColumnCount();
            ArrayList<String>  columns = new ArrayList<>();
            columns.add("#");
            for(int i=1; i<=columnCount; i++){
                columns.add(meta.getColumnLabel(i));
            }
            result.setColumns(columns);
            ObservableList<ObservableList<ResultCell>> rows = FXCollections.observableArrayList();
            int[] counter = {0};
            while(rs.next()){
                counter[0]++;
                ObservableList<ResultCell> row = FXCollections.observableArrayList();
                for(int i=0; i<=columnCount; i++){
                    String name = columns.get(i);
                    ResultCell cell;
                    if(name.equals("#")){
                        cell = new ResultCell(name, String.valueOf(counter[0]));
                        row.add(cell);
                        continue;
                    }
                    try {
                        Object ob = rs.getObject(i);

                        if(ob==null){
                            cell = new ResultCell(name,"[NULL]");
                        }else{
                            cell = new ResultCell(name, ob.toString());
                        }
                        row.add(cell);
                    } catch (SQLException throwables) {
                            System.out.println(throwables.getMessage());
                    }

                }

                rows.add(row);
            }
            result.setRows(rows);

        } catch (SQLException throwables) {
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "SQL Error", throwables.getMessage()));
        }
        return result;
    }

    private DataResult executeSelect(String sql){
        DataResult result = new DataResult();
        try {
            Statement stm = connection.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            ResultSetMetaData meta = rs.getMetaData();
            int columnCount = meta.getColumnCount();
            ArrayList<String>  columns = new ArrayList<>();
            columns.add("#");
            for(int i=1; i<=columnCount; i++){
                columns.add(meta.getColumnLabel(i));
            }
            result.setColumns(columns);
            ObservableList<ObservableList<ResultCell>> rows = FXCollections.observableArrayList();
            int[] counter = {0};
            while(rs.next()){
                counter[0]++;
                ObservableList<ResultCell> row = FXCollections.observableArrayList();

                for(int i=0; i<=columnCount; i++){
                    String name = columns.get(i);
                    ResultCell cell;
                    if(name.equals("#")){
                        cell = new ResultCell(name, String.valueOf(counter[0]));
                        row.add(cell);
                        continue;
                    }
                    try {
                        Object ob = rs.getObject(i);

                        if(ob==null){
                            cell = new ResultCell(name,"[NULL]");
                        }else{
                            cell = new ResultCell(name, ob.toString());
                        }
                        row.add(cell);
                    } catch (SQLException throwables) {
                        System.out.println(throwables.getMessage());
                    }

                }
                rows.add(row);
            }
            result.setRows(rows);

        } catch (SQLException throwables) {
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "SQL Error", throwables.getMessage()));
        }
        return result;
    }
    private DataResult executeUpdate(String sql){
        DataResult result = new DataResult();
        try{
            Statement stm = connection.createStatement();
            int xres = stm.executeUpdate(sql);
            ArrayList<String> columns = new ArrayList<>();
            columns.add("Type");
            columns.add("Affected Rows");
            result.setColumns(columns);
            String[] tokens = sql.trim().split(" ");

            ObservableList<ObservableList<ResultCell>> rows = FXCollections.observableArrayList();
            ObservableList<ResultCell> row = FXCollections.observableArrayList();
            row.add(new ResultCell("Type", tokens[0].toUpperCase(Locale.ROOT)));
            row.add(new ResultCell("Affected Rows", String.valueOf(xres)));
            rows.add(row);
            result.setRows(rows);

        }catch (Exception ex){
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "SQL Error", ex.getMessage()));
        }
        return result;
    }
    public DataResult executeSql(String sql){

        sql = sql.trim();
        String[] tokens = sql.split(" ");
        if(tokens[0].toLowerCase(Locale.ROOT).equals("select")){
            return executeSelect(sql);
        }else{
            return executeUpdate(sql);
        }

    }
}
