package com.sinxel.lairda.providers;

import com.sinxel.lairda.models.DataMap;
import com.sinxel.lairda.models.DataResult;

import java.sql.Connection;
import java.util.ArrayList;

public interface DatabaseConnection {
    public static final String ORACLE = "Oracle";
    public static final String POSTGRES = "PostgreSql";
    public static final String MYSQL = "MySQL";
    static DataMap getConnectionData(){
        return null;
    }
    Connection getConnection();
    ArrayList<String> getDatabases();
    ArrayList<String> getTables();
    DataResult getDefaultTableView(String tableName);
    DataResult executeSql(String sql);
}
