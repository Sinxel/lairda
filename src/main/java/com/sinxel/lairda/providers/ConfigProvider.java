package com.sinxel.lairda.providers;

import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class ConfigProvider {

    public static final String KEY_DATABASE_FILE = "dataFile";

    private static ConfigProvider _instance;
    private static JSONObject config;



    private ConfigProvider(){

    }

    public static ConfigProvider getInstance(){
        if(_instance==null) {
            loadConfigurations();
            _instance = new ConfigProvider();
        }

        return _instance;
    }

    void putString(String key, String value){

        config.put(key, value);
        try {
            Files.writeString(Paths.get("data/config.json"), config.toString(), StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getString(String key, String initialValue){
        if(config.has(key)){
            return config.getString(key);
        }
        putString(key, initialValue);
        return initialValue;
    }

    private static void loadConfigurations(){
        try {

            String json = Files.readString(Paths.get("data/config.json"));
            config = new JSONObject(json);

        } catch (IOException e) {

            createDefaultConfigurations();
        }
    }

    public static void createDefaultConfigurations(){
        Path path = Paths.get("data/config.json");

        config = new JSONObject();
        config.put("dataFile", "data/lairda.db");
        config.put("logFile", "data/lairda.log");
        try {
            Files.createDirectories(Paths.get("data"));
            Files.writeString(path, config.toString(4));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
