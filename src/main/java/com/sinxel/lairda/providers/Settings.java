package com.sinxel.lairda.providers;

import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.models.AppNotification;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Settings {
    public static final String KEY_DB_CONNECTIONS_CLUSTER_ID = "db_connections_cluster_id" ;
    public static String getSetting(String name){
        String sql = "SELECT value FROM settings WHERE name=? LIMIT 1;";

        try {
            PreparedStatement stm = Lairda.getConnection().prepareStatement(sql);
            stm.setString(1, name);
            ResultSet rs = stm.executeQuery();
            if(!rs.next()) return null;
            return rs.getString("value");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;

    }

    public static void setSetting(String key, String value){
        String currentValue = getSetting(key);
        String sql = "UPDATE settings SET value=? WHERE name=?;";
        if(currentValue==null)
            sql = "INSERT INTO settings (value, name) VALUES (?, ?);";
        try {
            PreparedStatement stm = Lairda.getConnection().prepareStatement(sql);
            stm.setString(1, value);
            stm.setString(2, key);
            stm.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void changePassword(String password){
        String sql = "PRAGMA rekey=?;";
        try {
            PreparedStatement stm = Lairda.getConnection().prepareStatement(sql);
            stm.setString(1, password);
            boolean changed = stm.execute();
            System.out.println(changed);
        }catch (Exception ex){
            ex.printStackTrace();
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Could not change password", ex.getMessage()));
        }
    }
}
