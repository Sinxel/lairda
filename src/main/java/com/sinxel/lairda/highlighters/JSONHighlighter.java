package com.sinxel.lairda.highlighters;

import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JSONHighlighter {
    static final String DOUBLE_QUOTED_KEY = "\"([^\"\\\\]|\\\\.)*\"(\s)?:";
    static final String SINGLE_QUOTED_KEY = "'([^\"\\\\]|\\\\.)*'(\s)?:";
    static final String DOUBLE_QUOTED_STRING = "\"([^\"\\\\]|\\\\.)*\"";
    static final String SINGLE_QUOTED_STRING = "'([^\"\\\\]|\\\\.)*'";
    static final String NUMBER = "[(\\d)+\\.(\\d)+]";
    static final String BOOLEAN = "(true|false|null)";

    String text;
    public JSONHighlighter(String text){

        this.text = text;

    }

    public StyleSpans<Collection<String>> highlight(){
        Matcher matcher = getPatterns().matcher(text);
        StyleSpansBuilder<Collection<String>> builder = new StyleSpansBuilder<>();
        int lastKwEnd = 0;
        while(matcher.find()){
            String styleClass = matcher.group("KEY")!=null?"key":
                    matcher.group("STRING")!=null?"string":
                            matcher.group("NUMBER")!=null?"number":
                                    matcher.group("BOOLEAN")!=null?"boolean":
                                            null;
            builder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            builder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        builder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return builder.create();
    }

    Pattern getPatterns(){
        String regex = "(?<KEY>" + DOUBLE_QUOTED_KEY + "|" + SINGLE_QUOTED_KEY +")";
        regex += "|(?<STRING>" + DOUBLE_QUOTED_STRING + "|" + SINGLE_QUOTED_STRING +")";
        regex += "|(?<NUMBER>" + NUMBER +")";
        regex += "|(?<BOOLEAN>" + BOOLEAN +")";

        return  Pattern.compile(regex);
    }

}
