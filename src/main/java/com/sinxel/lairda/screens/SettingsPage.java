package com.sinxel.lairda.screens;

import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.models.AppNotification;
import com.sinxel.lairda.providers.Settings;
import com.sinxel.lairda.providers.Sqlite;
import com.sinxel.lairda.values.Dimensions;
import com.sinxel.lairda.values.ICIcons;
import com.sinxel.lairda.widgets.IcoFont;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.controlsfx.control.textfield.CustomPasswordField;

public class SettingsPage extends VBox {
    public SettingsPage(){
        TitledPane pane = new TitledPane();
        pane.setText("Change Password");
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(Dimensions.SAFE_SPACE));
        grid.setVgap(Dimensions.SMALL_GAP);
        grid.setHgap(Dimensions.SMALL_GAP);
        Label lblPassword = new Label("New Password");
        CustomPasswordField txtPassword = new CustomPasswordField();
        txtPassword.setRight(new IcoFont(ICIcons.UI_PASSWORD));
        grid.add(lblPassword, 0, 0);
        grid.add(txtPassword, 1, 0);

        Label lblConfirmPassword = new Label("Confirm Password");
        CustomPasswordField txtConfirmPassword = new CustomPasswordField();
        txtConfirmPassword.setRight(new IcoFont(ICIcons.UI_PASSWORD));
        grid.add(lblConfirmPassword, 0, 1);
        grid.add(txtConfirmPassword, 1, 1);

        Button btnSave = new Button("Save");
        btnSave.setGraphic(new IcoFont(ICIcons.CHECK_CIRCLED));
        btnSave.getStyleClass().add("primary");
        btnSave.setOnAction(ev->{
            String password  = txtPassword.getText().trim();
            String confirmPassword = txtConfirmPassword.getText().trim();
            if(password.length()<6){
                Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Short Password", "Password must be at least 6 characters long."));
                return;
            }
            if(!password.equals(confirmPassword)){
                Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Password Mismatch", "Passwords are not the same."));
                return;
            }
            Sqlite.changePassword("data/lairda.db", "", password);
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.SUCCESS, "Password Changed", "Password has been changed"));
            txtPassword.setText("");
            txtConfirmPassword.setText("");
        });
        grid.add(btnSave, 0, 2);
        pane.setContent(grid);

        getChildren().add(pane);
    }
}
