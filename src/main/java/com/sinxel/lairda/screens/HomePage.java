package com.sinxel.lairda.screens;

import com.sinxel.lairda.values.Dimensions;
import com.sinxel.lairda.values.MdIcons;
import com.sinxel.lairda.widgets.CircleButton;
import com.sinxel.lairda.widgets.WidgetBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;

public class HomePage extends VBox {
    public HomePage(){
        setSpacing(Dimensions.SAFE_SPACE);
        createTopBar();
        createSmallWidgetsList();
    }

    void createTopBar(){
        HBox topBar = new HBox();
        topBar.getStyleClass().add("top-bar");
        topBar.setSpacing(10);
        TextField txtSearch = new TextField();
        txtSearch.setPromptText("Search");
        topBar.getChildren().add(txtSearch);
        HBox.setHgrow(txtSearch, Priority.ALWAYS);

        topBar.getChildren().add(new CircleButton(MdIcons.ACCOUNT_CIRCLE));
        getChildren().add(topBar);
    }

    void createSmallWidgetsList(){
        HBox widgetsList = new HBox();
        widgetsList.getChildren().add(new WidgetBox());
        getChildren().add(widgetsList);
    }
}
