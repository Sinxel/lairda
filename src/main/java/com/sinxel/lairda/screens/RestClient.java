package com.sinxel.lairda.screens;

import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.models.AppNotification;
import com.sinxel.lairda.screens.restTabs.RestResponsePage;
import com.sinxel.lairda.values.Dimensions;
import com.sinxel.lairda.widgets.FullProgressIndicator;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import okhttp3.*;
import org.controlsfx.control.textfield.CustomTextField;

import java.net.URI;


public class RestClient extends VBox {
    CustomTextField txtAddress;
    SplitMenuButton mnuMethod;
    Node currentNode;
    public enum RequestMethod{
        GET,
        POST,
        DELETE,
        PATCH,
        PUT
    }
    RequestMethod method = RequestMethod.GET;
    public RestClient(){
        setSpacing(10);
        setupAddressBar();
    }
    void setupAddressBar(){
        HBox addressBox = new HBox();
        txtAddress = new CustomTextField();
        txtAddress.setPromptText("URL:");

        txtAddress.setOnKeyPressed(ev->{
            if(ev.getCode()== KeyCode.ENTER){
                fetchApi();
            }
        });
        mnuMethod =new SplitMenuButton();
        mnuMethod.setOnAction(ev->{
            fetchApi();
        });
        mnuMethod.setText(method.toString());
        for (RequestMethod rm: RequestMethod.values()) {
            MenuItem mniMethod = new MenuItem(rm.toString());
            mniMethod.setOnAction((ev)->{
                setMethod(rm);
            });
            mnuMethod.getItems().add(mniMethod);
        }
        addressBox.setSpacing(Dimensions.SMALL_GAP);
        HBox.setHgrow(txtAddress, Priority.ALWAYS);
        addressBox.getChildren().addAll(txtAddress, mnuMethod);

        getChildren().add(addressBox);
    }
    void setMethod(RequestMethod method){
        this.method = method;
        mnuMethod.setText(method.toString());
    }
    void fetchApi(){
        String url = txtAddress.getText().trim();
        if(url.isEmpty()) return;
        fetchApi(url);
    }

    void fetchApi(String url){

        showChild(new FullProgressIndicator());
        URI uri;
        try{
            uri = URI.create(url);
            System.out.println(uri.getScheme());
            if(uri.getScheme()==null){
                url = "http://" + url;
                txtAddress.setText(url);
                //Lairda.getConsole().warning("[Rest Client] URL Scheme Error", ex.getMessage());
                fetchApi(url);
                return;
            }
        }catch (IllegalArgumentException ex){
            url = "http://" + url;
            txtAddress.setText(url);
            //Lairda.getConsole().warning("[Rest Client] URL Scheme Error", ex.getMessage());
            fetchApi(url);
            return;
        }catch (Exception ex){

            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Could complete request", ex.getMessage()));
            return;
        }
        sendRequest(uri);
    }
    void showChild(Node child){
        if(currentNode!=null) {
            getChildren().remove(currentNode);
        }
        if(child==null) return;
        VBox.setVgrow(child, Priority.ALWAYS);
        currentNode = child;
        getChildren().add(currentNode);
    }


    void sendRequest(URI uri){

        try{
            OkHttpClient client = new OkHttpClient();
            Request.Builder builder = new Request.Builder()
                    .url(uri.toString());
            builder.header("accept", "application/json");
            builder.get();
            Request request = builder.build();
            Response response = client.newCall(request).execute();
            handleResponse(response);
        }catch (Exception ex){
            Lairda.getInstance().showNotification(new AppNotification(AppNotification.Type.ERROR, "Request Error", ex.getMessage() ));
            showChild(null);
        }
    }

    void handleResponse(Response response){
        Platform.runLater(()->{
            RestResponsePage restResponse = new RestResponsePage(response);
            showChild(restResponse);
        });
    }
}
