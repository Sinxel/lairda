package com.sinxel.lairda.screens;


import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.models.DataMap;
import com.sinxel.lairda.models.Datum;
import com.sinxel.lairda.providers.MySQL;
import com.sinxel.lairda.values.Dimensions;
import com.sinxel.lairda.values.MdIcons;
import com.sinxel.lairda.widgets.SideMenuButton;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;


public class HomeScreen extends AnchorPane {
    Node currentPage;
    HomePage homePage;
    DatabasePage databasePage;
    RestClient restClient;
    SettingsPage settingsPage;
    public HomeScreen(){
        buildSideMenu();

        setPage(new HomePage());
    }



    void setPage(Node node){
        if(currentPage!=null){
            getChildren().remove(currentPage);
        }
        currentPage = node;
        AnchorPane.setBottomAnchor(currentPage, Dimensions.SAFE_SPACE);
        AnchorPane.setLeftAnchor(currentPage, Dimensions.SAFE_SPACE*2+Dimensions.SIDE_MENU);
        AnchorPane.setTopAnchor(currentPage, Dimensions.SAFE_SPACE);
        AnchorPane.setRightAnchor(currentPage, Dimensions.SAFE_SPACE);
        getChildren().add( currentPage);



    }

    void buildSideMenu(){
        VBox sideMenu = new VBox();
        sideMenu.setSpacing(Dimensions.SIDE_MENU_SPACING);
        sideMenu.setAlignment(Pos.CENTER);
        sideMenu.setMinWidth(Dimensions.SIDE_MENU);
        sideMenu.setMaxWidth(Dimensions.SIDE_MENU);
        sideMenu.getStyleClass().add("side-menu");
        AnchorPane.setTopAnchor(sideMenu, Dimensions.SAFE_SPACE);
        AnchorPane.setLeftAnchor(sideMenu, Dimensions.SAFE_SPACE);
        AnchorPane.setBottomAnchor(sideMenu, Dimensions.SAFE_SPACE);

        //adding menu items
        Image logo = new Image("file:assets/images/logo_light.png", Dimensions.LOGO_SIZE, Dimensions.LOGO_SIZE, true, true);
        ImageView btnLogo = new ImageView(logo);
        btnLogo.setOnMouseClicked((ev)->{
            Lairda.getInstance().loadTheme();
        });

        SideMenuButton btnHome = new SideMenuButton(MdIcons.HOME, "Home");
        SideMenuButton btnDatabases = new SideMenuButton(MdIcons.STORAGE, "Databases");
        SideMenuButton btnSettings = new SideMenuButton(MdIcons.SETTINGS, "Settings");
        SideMenuButton btnRestClient = new SideMenuButton(MdIcons.CODE, "Rest Client");
        SideMenuButton btnWebClient = new SideMenuButton(MdIcons.LANGUAGE, "Web Client");
        SideMenuButton btnClusters = new SideMenuButton(MdIcons.GROUP_WORK, "Clusters");

        btnHome.setOnMouseClicked((ev)->{
            if(homePage == null){
                homePage = new HomePage();
            }
            setPage(homePage);
        });
        btnDatabases.setOnMouseClicked((ev)->{
            if(databasePage==null){
                databasePage = new DatabasePage();
            }
            setPage(databasePage);
        });

        btnRestClient.setOnMouseClicked((ev)->{
            if(restClient==null){
                restClient = new RestClient();
            }
            setPage(restClient);
        });
        btnSettings.setOnMouseClicked(ev->{
            if(settingsPage==null){
                settingsPage = new SettingsPage();
            }
            setPage(settingsPage);
        });
        sideMenu.getChildren().add(btnLogo);
        Region spacer = new Region();
        spacer.setMinHeight(5);
        spacer.setMaxHeight(5);
        sideMenu.getChildren().add(spacer);
        sideMenu.getChildren().add(btnHome);
        sideMenu.getChildren().add(btnClusters);
        sideMenu.getChildren().add(btnRestClient);
        sideMenu.getChildren().add(btnWebClient);
        sideMenu.getChildren().add(btnDatabases);
        sideMenu.getChildren().add(btnSettings);

        getChildren().add(sideMenu);
    }
}
