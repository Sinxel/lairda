package com.sinxel.lairda.screens.dataTabs;

import com.sinxel.lairda.factories.DataTableCell;
import com.sinxel.lairda.models.DataResult;
import com.sinxel.lairda.models.ResultCell;
import com.sinxel.lairda.providers.DatabaseConnection;
import com.sinxel.lairda.providers.Postgres;
import com.sinxel.lairda.values.Colors;
import com.sinxel.lairda.values.Dimensions;
import com.sinxel.lairda.values.MdIcons;
import com.sinxel.lairda.widgets.FullProgressIndicator;
import com.sinxel.lairda.widgets.IconButton;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

public class SqlTab extends Tab {
    String tableName;
    TextArea txtSql;
    DatabaseConnection connection;
    VBox content;
    Node currentChild;

    public SqlTab(String title, DatabaseConnection connection){
        super(title);
        this.connection = connection;
        init();
    }

    public SqlTab(String title, String tableName, DatabaseConnection connection){
        super(title);
        this.tableName = tableName;
        this.connection = connection;
        init();
    }

    void init(){
        content = new VBox();
        content.getStyleClass().add("data-tab");
        content.setSpacing(Dimensions.SMALL_GAP);
        txtSql = new TextArea();
        txtSql.setMaxHeight(140);
        txtSql.setMinHeight(140);
        content.getChildren().add(txtSql);

        createToolsBox(content);

        setContent(content);
        if(!tableName.trim().equals("")){
            showTable();
        }
    }

    void createToolsBox(VBox parent){
        HBox toolsContainer = new HBox();
        toolsContainer.getStyleClass().add("tools-box");

        IconButton btnRun = new IconButton(MdIcons.PLAY_CIRCLE_FILLED, Dimensions.ICON_BUTTON_SIZE, Colors.GREEN);
        btnRun.setOnMouseClicked((ev)->{
            executeSql();
        });
        toolsContainer.getChildren().add(btnRun);
        parent.getChildren().add(toolsContainer);
    }
    void showTable(){
        DataResult result = connection.getDefaultTableView(tableName);
        populateResults(result);
    }
    void executeSql(){
        setChild(new FullProgressIndicator());
        String sql = txtSql.getText().trim();
        if(sql.equals("")) return;
        DataResult result = connection.executeSql(sql);
        populateResults(result);
    }

    void populateResults(DataResult result){
       TableView<ObservableList<ResultCell>> tblRecords = new TableView<ObservableList<ResultCell>>();
       if(result.getColumns()==null){
           setChild(new Region());
           return;
       }
       result.getColumns().forEach((columnName)->{
           TableColumn<ObservableList<ResultCell>, String> column = new TableColumn(columnName);

           column.setCellValueFactory(param -> {
               ObservableList<ResultCell> row = param.getValue();

               for (int i = 0; i < row.size(); i++) {
                   String name = row.get(i).getName();
                   if (name.equals(columnName)) {
                       return new SimpleStringProperty(row.get(i).getValue());
                   }
               }

               return new SimpleStringProperty("[NULL]");

           });
           column.setCellFactory(table->new DataTableCell());
           tblRecords.getColumns().add(column);
       });
       tblRecords.setItems(result.getRows());

       setChild(tblRecords);


    }

    ContextMenu getCellContextMenu(String name){
        ContextMenu mnuOptions = new ContextMenu();
        MenuItem mnuCopyValue = new MenuItem("Copy Value");
        mnuCopyValue.setOnAction(ev->System.out.println(name));
        mnuOptions.getItems().add(mnuCopyValue);
        return mnuOptions;
    }

    void setChild(Node child){
        if(currentChild!=null){
            content.getChildren().remove(currentChild);

        }
        VBox.setVgrow(child, Priority.ALWAYS);
        content.getChildren().add(child);
        currentChild = child;

    }


}
