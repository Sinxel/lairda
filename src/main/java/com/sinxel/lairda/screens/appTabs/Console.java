package com.sinxel.lairda.screens.appTabs;

import javafx.scene.control.TextArea;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class Console extends VBox {
    TextArea txtConsole;
    public Console(){
        txtConsole = new TextArea();
        txtConsole.setWrapText(true);
        VBox.setVgrow(txtConsole, Priority.ALWAYS);
        getChildren().add(txtConsole);

    }

    public void appendMessage(String message){
        txtConsole.appendText(message+"\n");
    }
}
