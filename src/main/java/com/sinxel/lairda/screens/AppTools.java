package com.sinxel.lairda.screens;

import com.sinxel.lairda.screens.appTabs.Console;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class AppTools extends TabPane {
    public static Console console;
    public AppTools(){
        setMaxHeight(340);
        setMinHeight(340);
        getStyleClass().add("app-tools");
        Tab consoleTab = new Tab("Console");
        console = new Console();
        consoleTab.setContent(console);
        consoleTab.setClosable(false);
        getTabs().add(consoleTab);

    }

}
