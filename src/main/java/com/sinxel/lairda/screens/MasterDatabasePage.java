package com.sinxel.lairda.screens;

import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.models.AppNotification;
import com.sinxel.lairda.providers.ConfigProvider;
import com.sinxel.lairda.providers.Sqlite;
import com.sinxel.lairda.values.ICIcons;
import com.sinxel.lairda.widgets.AlertCard;
import com.sinxel.lairda.widgets.IcoFont;
import com.sinxel.lairda.widgets.VSpace;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import org.controlsfx.control.textfield.CustomPasswordField;
import org.controlsfx.control.textfield.CustomTextField;

import java.util.Objects;


public class MasterDatabasePage extends VBox {
    public enum Status{
        NEW_DATABASE,
        OPEN_DATABASE
    }
    Status status;
    CustomPasswordField txtPassword;
    CustomPasswordField txtConfirmPassword;
    public MasterDatabasePage(Status status){
        this.status = status;
        setAlignment(Pos.CENTER);
        setSpacing(10);
        Image logo = new Image("file:assets/images/logo.png", 154, 154, true, true);
        getChildren().add(new ImageView(logo));
        Label lblBrand = new Label("Lairda");
        lblBrand.setOnMouseClicked((ev)-> Lairda.getInstance().loadTheme());
        lblBrand.getStyleClass().add("brand");
        getChildren().add(lblBrand);
        String warningMessage = "Lairda utilizes an encrypted master database to store all your data and configurations, please pick a strong password, write it down, and never lose it, as we cannot retrieve your data if you lost it.";
        if(status==Status.OPEN_DATABASE){
            warningMessage = "Password is required to open the master database.";
        }
        AlertCard alert = new AlertCard(new AppNotification(status==Status.NEW_DATABASE? AppNotification.Type.WARNING:AppNotification.Type.INFO, status==Status.NEW_DATABASE?"No Master Database":"Password Required", warningMessage));
        alert.setMaxWidth(460);
        getChildren().add(alert);

        getChildren().add(new VSpace(10));

        txtPassword = new CustomPasswordField();
        txtPassword.setPromptText("Password");
        txtPassword.setRight(new IcoFont(ICIcons.UI_PASSWORD));
        txtPassword.setMaxWidth(300);
        txtPassword.setOnAction(ev->unlock());
        getChildren().add(txtPassword);

        if(status==Status.NEW_DATABASE){
            txtConfirmPassword = new CustomPasswordField();
            txtConfirmPassword.setPromptText("Retype Password");
            txtConfirmPassword.setRight(new IcoFont(ICIcons.UI_PASSWORD));
            txtConfirmPassword.setMaxWidth(300);
            txtConfirmPassword.setOnAction(ev->unlock());
            getChildren().add(txtConfirmPassword);
        }


        Button btnSave = new Button(status==Status.NEW_DATABASE?"Save":"Open");
        btnSave.setGraphic(new IcoFont(ICIcons.CHECK_CIRCLED));
        btnSave.getStyleClass().add("success");
        btnSave.setOnAction(ev->unlock());
        getChildren().add(new VSpace(10));
        getChildren().add(btnSave);
    }

    void unlock(){
        if(status==Status.NEW_DATABASE){
            saveDatabase();
        }else{
            unlockDatabase();
        }
    }

    void saveDatabase(){
        if(txtPassword.getText().trim().length()<6){
            Alert alert = new Alert(Alert.AlertType.ERROR, "Password must be at least 6 characters long");
            alert.show();
            return;
        }
        if(!Objects.equals(txtPassword.getText(), txtConfirmPassword.getText())){
            Alert alert = new Alert(Alert.AlertType.ERROR, "Passwords don't match");
            alert.show();
            return;
        }
        Sqlite.createDefaultDatabase(ConfigProvider.getInstance().getString(ConfigProvider.KEY_DATABASE_FILE, "data/lairda.db"), txtPassword.getText());
        unlockDatabase();

    }
    void unlockDatabase(){
        Sqlite connection = new Sqlite(ConfigProvider.getInstance().getString(ConfigProvider.KEY_DATABASE_FILE, "data/lairda.db"), txtPassword.getText());
        if(!connection.isOpen()){
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please make sure you have typed the correct password.");
            alert.show();
            return;
        }
        Lairda.getInstance().setDatabase(connection);
    }
}
