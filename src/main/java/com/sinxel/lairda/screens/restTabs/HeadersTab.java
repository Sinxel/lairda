package com.sinxel.lairda.screens.restTabs;

import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import okhttp3.Response;

public class HeadersTab extends Tab {
    TextArea txtHeaders = new TextArea();
    public HeadersTab(Response response){
        setText("Response Headers");
        setClosable(false);
        response.headers().forEach((pair)->{
            txtHeaders.appendText(pair.getFirst()+" : " + pair.getSecond()+"\n");
        });
        setContent(txtHeaders);
    }
}
