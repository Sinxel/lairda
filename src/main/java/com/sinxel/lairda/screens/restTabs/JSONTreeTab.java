package com.sinxel.lairda.screens.restTabs;

import javafx.scene.control.Tab;
import okhttp3.Response;

public class JSONTreeTab  extends Tab {
    public JSONTreeTab(Response response){
        setText("JSON Tree");
        setClosable(false);
    }
}
