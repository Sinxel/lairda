package com.sinxel.lairda.screens.restTabs;

import com.sinxel.lairda.widgets.CodeEditor;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import okhttp3.Response;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

public class RestResponsePage extends TabPane {
    public RestResponsePage(Response response){
        getTabs().add(new JSONTreeTab(response));
        Tab codeTab = new Tab("Raw Response");
        CodeEditor editor = new CodeEditor(CodeEditor.Syntax.JSON);
        try {
            String json = new JSONObject(response.body().string()).toString(4);
            editor.setText(json);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        codeTab.setContent(editor);
        codeTab.setClosable(false);
        getTabs().add(codeTab);
        getTabs().add(new HeadersTab(response));
    }
}
