package com.sinxel.lairda.screens;

import javafx.geometry.Side;
import javafx.scene.control.Label;
import org.controlsfx.control.HiddenSidesPane;
import org.controlsfx.control.MasterDetailPane;

public class AppHost extends HiddenSidesPane {
    AppTools tools;
    public AppHost(){
        setContent(new HomeScreen());
        tools = new AppTools();
        setBottom(tools);

    }

    public void toggleAppTools(){
        if(getPinnedSide()==null){
            setPinnedSide(Side.BOTTOM);
        }else{
            setPinnedSide(null);
        }
    }

    public boolean areToolsVisible(){
        return getPinnedSide()!=null;
    }
}
