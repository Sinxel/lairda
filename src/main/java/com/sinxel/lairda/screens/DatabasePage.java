package com.sinxel.lairda.screens;

import com.sinxel.lairda.dialogs.ClusterPicker;
import com.sinxel.lairda.dialogs.FormDialog;
import com.sinxel.lairda.factories.DatabaseCellFactory;
import com.sinxel.lairda.models.Cluster;
import com.sinxel.lairda.models.DatabaseItemView;
import com.sinxel.lairda.models.Datum;
import com.sinxel.lairda.models.DataMap;
import com.sinxel.lairda.providers.*;
import com.sinxel.lairda.screens.dataTabs.SqlTab;
import com.sinxel.lairda.services.ClusterManager;
import com.sinxel.lairda.values.ICIcons;
import com.sinxel.lairda.widgets.IcoFont;
import com.sinxel.lairda.widgets.TitleLabel;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Optional;

public class DatabasePage extends SplitPane {

    TabPane dataTabs;
    ArrayList<Cluster> connections;
    TreeView<DatabaseItemView> tvwConnections;

    public DatabasePage() {
        buildSideTree();
        setDividerPosition(0, 0.20);
        buildDataView();
        loadConnections();

    }

    void loadConnections() {
        String storedClusterId = Settings.getSetting(Settings.KEY_DB_CONNECTIONS_CLUSTER_ID);
        int clusterId;
        if (storedClusterId == null) {

            DataMap data = new DataMap();
            data.add(new Datum("title", "_DB Connections_"));
            data.add(new Datum("text", "This cluster was auto generated by Lairda to store Database Connections, for the Database Manager."));
            clusterId = ClusterManager.insertCluster(0, data, false);
            Settings.setSetting(Settings.KEY_DB_CONNECTIONS_CLUSTER_ID, String.valueOf(clusterId));
        } else {
            clusterId = Integer.parseInt(storedClusterId);
        }
        connections = ClusterManager.getAllClusters(clusterId);
        populateTree();


    }

    void buildDataView() {
        dataTabs = new TabPane();
        getItems().add(dataTabs);
    }

    void buildSideTree() {
        VBox connectionsBox = new VBox();
        connectionsBox.getStyleClass().add("tree-host");
        connectionsBox.setSpacing(10);
        createTitleBox(connectionsBox);

        tvwConnections = new TreeView<>();
        tvwConnections.setCellFactory(tree -> new DatabaseCellFactory());
        tvwConnections.setOnMouseClicked((ev) -> {
            if (ev.getClickCount() == 2) {
                TreeItem<DatabaseItemView> selectedItem = tvwConnections.getSelectionModel().getSelectedItem();
                switch (selectedItem.getValue().getType()) {
                    case CONNECTION -> connectTo(selectedItem);
                    case DATABASE -> {

                        DataMap data = selectedItem.getValue().getData();
                        DatabaseConnection connection = createDatabaseConnection(data, selectedItem.getValue().getDataBaseName());

                        ArrayList<String> tables = connection.getTables();
                        tables.forEach((table) -> {
                            DatabaseItemView divTable = new DatabaseItemView(DatabaseItemView.Type.TABLE, table, selectedItem.getValue().getName(), connection, null);
                            TreeItem<DatabaseItemView> item = new TreeItem<>(divTable);
                            selectedItem.getChildren().add(item);
                            divTable.setMenu(getTableMenu());
                        });
                        selectedItem.setExpanded(true);
                    }
                    case TABLE -> showTable(selectedItem);
                }

            }
        });


        VBox.setVgrow(tvwConnections, Priority.ALWAYS);
        connectionsBox.getChildren().add(tvwConnections);

        getItems().add(connectionsBox);
    }

    void showTable(TreeItem<DatabaseItemView> item) {
        DatabaseConnection connection = item.getValue().getConnection();
        String tableName = item.getValue().getName();

        SqlTab resultsTab = new SqlTab(item.getValue().getDataBaseName() + "." + tableName, tableName, connection);
        dataTabs.getTabs().add(resultsTab);
        dataTabs.getSelectionModel().select(resultsTab);
    }

    ContextMenu getDatabaseMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem mniSqlTab = new MenuItem("SQL Tab");
        MenuItem mniStatus = new MenuItem("Information");
        menu.getItems().addAll(mniSqlTab, mniStatus);
        return menu;
    }

    ContextMenu getTableMenu(){
        ContextMenu menu =new ContextMenu();
        MenuItem mniOpen = new MenuItem("Open");
        MenuItem mniInformation = new MenuItem("Information");
        MenuItem mniSqlTab = new MenuItem("SQL Tab");
        menu.getItems().addAll(mniOpen, mniSqlTab, mniInformation);
        return menu;

    }
    ContextMenu getConnectionMenu(boolean isClosed) {
        ContextMenu menu = new ContextMenu();
        MenuItem mniDeleteConnection = new MenuItem("Delete Connection");
        MenuItem mniOpenConnection = new MenuItem("Open Connection");
        MenuItem mniCloseConnection = new MenuItem("Close Connection");
        MenuItem mniEditConnection = new MenuItem("Edit Connection");
        MenuItem mniConnectionStatus = new MenuItem("Status");

        if(isClosed){
            menu.getItems().add(mniOpenConnection);
        }else{
            menu.getItems().addAll(mniCloseConnection,  mniConnectionStatus);
        }
        menu.getItems().addAll(mniEditConnection, new SeparatorMenuItem(), mniDeleteConnection);


        return  menu;
    }
    void createTitleBox(VBox parent){
        HBox connectionsTools = new HBox();
        connectionsTools.setAlignment(Pos.CENTER_LEFT);
        TitleLabel lblTitle = new TitleLabel("Connections");
        MenuButton btnConnections = new MenuButton();
        btnConnections.setGraphic(new IcoFont(ICIcons.PLUS_CIRCLE));
        Pane spacer = new Pane();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        connectionsTools.getChildren().addAll(lblTitle, spacer,  btnConnections);
        MenuItem mnuAddOracle = new MenuItem("Oracle Connection");
        MenuItem mnuAddPostgres = new MenuItem("PostgreSql Connection");
        MenuItem mnuAddMySQL = new MenuItem("MySQL Connection");
        mnuAddOracle.setOnAction(ev->createNewConnection(DatabaseConnection.ORACLE));
        mnuAddPostgres.setOnAction(ev->createNewConnection(DatabaseConnection.POSTGRES));
        mnuAddMySQL.setOnAction(ev->createNewConnection(DatabaseConnection.MYSQL));
        btnConnections.getItems().addAll(mnuAddOracle, mnuAddPostgres, mnuAddMySQL);


        parent.getChildren().add(connectionsTools);
    }

    void createNewConnection(String connectionType){

        String connectionsCluster = Settings.getSetting(Settings.KEY_DB_CONNECTIONS_CLUSTER_ID);
        if(connectionsCluster==null){
            ClusterPicker clusterPicker = new ClusterPicker();
            Optional<Integer> result = clusterPicker.showAndWait();
            result.ifPresent(cluster->addConnection(cluster, connectionType));
        }else{
            int id = Integer.parseInt(connectionsCluster);
            addConnection(id, connectionType);
        }
    }
    void populateTree(){
        TreeItem<DatabaseItemView> root = new TreeItem<>();
        root.setValue(new DatabaseItemView(DatabaseItemView.Type.ROOT, "Root", null, new DataMap()));
        root.setExpanded(true);
        tvwConnections.setRoot(root);
        tvwConnections.setShowRoot(false);
        connections.forEach((cluster)->{
            DataMap data = cluster.getData();
            DatabaseItemView divConnection = new DatabaseItemView(DatabaseItemView.Type.CONNECTION,  data.get("title").getValue(), null,  data);
            TreeItem<DatabaseItemView> tiConnection = new TreeItem<>(divConnection);
            divConnection.setMenu(getConnectionMenu(true));
            root.getChildren().add(tiConnection);
        });
    }
    void addConnection(int id, String connectionType){

        DataMap sortedFields = new DataMap();
        sortedFields.add(new Datum(Datum.Type.ReadOnly, "connectionType", connectionType).with(new JSONObject("{isRequired: true}")));
        if(connectionType.equals(DatabaseConnection.ORACLE)){
            DataMap fields = OracleDB.getConnectionData();
            sortedFields.add(new Datum("title", "Oracle").with(new JSONObject("{isRequired: true}")));
            sortedFields.putAll(fields);
        }else if(connectionType.equals(DatabaseConnection.POSTGRES)){
            DataMap fields = Postgres.getConnectionData();
            sortedFields.add(new Datum("title", DatabaseConnection.POSTGRES).with(new JSONObject("{isRequired: true}")));
            sortedFields.putAll(fields);
        }else if(connectionType.equals(DatabaseConnection.MYSQL)){
            DataMap fields = MySQL.getConnectionData();
            sortedFields.add(new Datum("title", DatabaseConnection.MYSQL).with(new JSONObject("{isRequired: true}")));
            sortedFields.putAll(fields);
        }
        FormDialog connectionSettings = new FormDialog("New Connection", sortedFields);
        Optional<DataMap> result = connectionSettings.showAndWait();
        result.ifPresent((data) -> {
            int connectionId = ClusterManager.insertCluster(id, data);
            Cluster connectionCluster = ClusterManager.getCluster(connectionId);
            if(connectionCluster!=null){
                DataMap clusterData = connectionCluster.getData();
                TreeItem<DatabaseItemView> item = new TreeItem<>(new DatabaseItemView(DatabaseItemView.Type.CONNECTION, clusterData.get("title").getValue(), null, clusterData ));
                tvwConnections.getRoot().getChildren().add(item);
                connectTo(item);
            }
        });

    }
    DatabaseConnection createDatabaseConnection(DataMap data, String databaseName){
        if(data.valueOf("connectionType").equals(DatabaseConnection.ORACLE)){
            return new OracleDB(data);
        }else if(data.valueOf("connectionType").equals(DatabaseConnection.POSTGRES)){
            return new Postgres(data.valueOf("host"), data.valueOf("port"), databaseName, data.valueOf("user"), data.valueOf("password"));
        }else{
            data.add(new Datum("database", databaseName));
            System.out.println(data);
            return new MySQL(data);
        }
    }
    void connectTo(TreeItem<DatabaseItemView> item){
        if(item.getValue().getConnection()!=null) return;
        DatabaseConnection connection = null;
        DataMap data = item.getValue().getData();
        if(data.get("connectionType").getValue().equals(DatabaseConnection.ORACLE)){
            connection = new OracleDB(data);
        }else if(data.get("connectionType").getValue().equals(DatabaseConnection.POSTGRES)){
            connection = new Postgres(data.valueOf("host"), data.valueOf("port"), "?", data.valueOf("user"), data.valueOf("password"));
        }else if(data.get("connectionType").getValue().equals(DatabaseConnection.MYSQL)){
            data.add(new Datum("database", ""));
            connection = new MySQL(data);
        }
        if(connection==null) {
            System.err.println("Connection failed");
            return;
        }else{
            System.out.println("Connection Success");
        }
        final DatabaseConnection con = connection;
        connection.getDatabases().forEach(db->{
            System.out.println(db);
            DatabaseItemView divDatabase = new DatabaseItemView(DatabaseItemView.Type.DATABASE, db, db, con, data);
            TreeItem<DatabaseItemView> databaseItem = new TreeItem<>(divDatabase);
            item.getChildren().add(databaseItem);
            divDatabase.setMenu(getDatabaseMenu());
        });

        item.setExpanded(true);
        item.getValue().setConnection(connection);
        item.getValue().setMenu(getConnectionMenu(false));
    }
}
