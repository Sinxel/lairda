package com.sinxel.lairda;
import com.sinxel.lairda.models.AppNotification;
import com.sinxel.lairda.providers.ConfigProvider;
import com.sinxel.lairda.providers.Sqlite;
import com.sinxel.lairda.screens.AppHost;
import com.sinxel.lairda.screens.AppTools;
import com.sinxel.lairda.screens.MasterDatabasePage;
import com.sinxel.lairda.values.ICIcons;
import com.sinxel.lairda.widgets.IcoFont;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.controlsfx.control.NotificationPane;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;

public class Lairda extends Application {
    Stage stage;
    Scene root;
    NotificationPane topParent;
    AppHost host;
    Sqlite masterDatabase;
    private static Lairda _instance;
    private String themeFile = "file:assets/themes/lairk.css";
    public static void main(String[] args){
        launch(args);
    }
    public static  Lairda getInstance(){
        return  _instance;
    }
    @Override
    public void start(Stage primaryStage) {
        if(_instance==null) _instance = this;
        this.stage = primaryStage;
        stage.setTitle("Lairda");
        stage.getIcons().add(new Image("file:assets/images/icon.png"));

        checkMasterDatabase();
    }




    void checkMasterDatabase(){
        String dataFile = ConfigProvider.getInstance().getString(ConfigProvider.KEY_DATABASE_FILE, "data/lairda.db");
        if(!Files.exists(Paths.get(dataFile))){
            initMasterDatabaseGUI(MasterDatabasePage.Status.NEW_DATABASE);
        }else{
            initMasterDatabaseGUI(MasterDatabasePage.Status.OPEN_DATABASE);
        }

    }

    public void setDatabase(Sqlite connection){
        masterDatabase = connection;

        initMainGui();
    }

    public static Connection getConnection(){
        return _instance.masterDatabase.getConnection();
    }
    void initMasterDatabaseGUI(MasterDatabasePage.Status status){
        root = new Scene(new MasterDatabasePage(status), 1280, 720);
        loadTheme();
        stage.setScene(root);
        stage.show();
    }
    void initMainGui(){
        host = new AppHost();
        topParent = new NotificationPane(host);
        topParent.getStyleClass().add("notification-pane");
        root = new Scene(topParent, 1280, 720);
        loadTheme();
        stage.setScene(root);
        stage.show();
    }

    public void showNotification(AppNotification notification){

        IcoFont icon = null;
        switch (notification.getType()){

            case ERROR -> {
                icon = new IcoFont(ICIcons.ERROR);
                icon.getStyleClass().add("error");

            }
            case INFO -> {
                icon = new IcoFont(ICIcons.INFO);
                icon.getStyleClass().add("info");
            }
            case WARNING -> {
                icon = new IcoFont(ICIcons.WARNING);
                icon.getStyleClass().add("warning");
            }
            case SUCCESS ->{
                icon = new IcoFont(ICIcons.CHECK);
                icon.getStyleClass().add("success");
            }

        }
        topParent.setGraphic(icon);
        topParent.setText(notification.getMessage());
        topParent.show();
    }
    public void toggleAppTools(){
        host.toggleAppTools();
    }
    public void loadTheme(){
        root.getStylesheets().clear();
        root.getStylesheets().add(themeFile);

    }

    public void appendToConsole(String message){
        if(!host.areToolsVisible()){
            toggleAppTools();
        }
        AppTools.console.appendMessage(message);

    }

    public String getTheme() {
        return themeFile;
    }
}
