package com.sinxel.lairda.models;

import com.sinxel.lairda.providers.DatabaseConnection;
import com.sinxel.lairda.providers.Postgres;
import javafx.scene.control.ContextMenu;

import java.util.HashMap;

public class DatabaseItemView {
    public enum Type{
        ROOT,
        CONNECTION,
        DATABASE,
        TABLE
    }



    Type type;
    String name;
    DatabaseConnection connection;
    String dataBaseName;
    DataMap data;
    ContextMenu menu;


    public DatabaseItemView(Type type, String name, DatabaseConnection connection, DataMap data){
        this.type = type; this.name = name; this.connection = connection; this.data  = data;
    }

    public DatabaseItemView(Type type, String name, String databaseName, DatabaseConnection connection, DataMap data){
        this.type = type; this.name = name; this.connection = connection;
        this.dataBaseName = databaseName;
        this.data = data;
    }

    public Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public DatabaseConnection getConnection() {
        return connection;
    }

    public String getDataBaseName() {
        return dataBaseName;
    }

    public DataMap getData() {
        return data;
    }

    public void setConnection(DatabaseConnection connection) {
        this.connection = connection;
    }
    public void setMenu(ContextMenu menu){
        this.menu = menu;
    }

    public ContextMenu getMenu(){return menu;}
}
