package com.sinxel.lairda.models;



import java.util.LinkedHashMap;


public class DataMap extends LinkedHashMap<String, Datum> {
    public void add(Datum datum){
        if(datum.getKey()==null) return;
        put(datum.getKey(), datum);
    }

    public String valueOf(String key){
        return get(key).getValue();
    }
}
