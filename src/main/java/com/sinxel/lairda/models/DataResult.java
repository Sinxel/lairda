package com.sinxel.lairda.models;

import javafx.collections.ObservableList;

import java.util.List;

public class DataResult {
    List<String> columns;
    ObservableList<ObservableList<ResultCell>> rows;

    public DataResult(){

    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public ObservableList<ObservableList<ResultCell>> getRows() {
        return rows;
    }

    public void setRows(ObservableList<ObservableList<ResultCell>> rows) {
        this.rows = rows;
    }
}
