package com.sinxel.lairda.models;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Datum {
    public static Type parseType(String type) {
        return Type.valueOf(type);
    }

    public enum Type{
        Text,
        Date,
        Number,
        Icon,
        Color,
        List,
        Boolean,
        Markup,
        ReadOnly,
        Secret
    }
    private Type type;
    private String key;
    private String value;
    private JSONObject extra;
    public Datum(){
        this.type = Type.Text;
    }

    public Datum(String key){
        this.key = key;
        this.type = Type.Text;
        this.value = null;
    }
    public Datum(Type type, String key){
        this.type = type;
        this.key = key;
        this.value = null;
    }
    public Datum(String key, String value){
        this.type = Type.Text;
        this.key = key; this.value = value;
    }

    public Datum(Type type, String key, String value){
        this.type = type; this.key = key; this.value = value;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public String getKeyLabel(){
        ArrayList<String> words = new ArrayList<>();
        String word = "";
        for(int i=0; i<key.length(); i++){

            char ch = key.charAt(i);
            if(Character.isUpperCase(ch)){
                if(word.length()>0&&Character.isUpperCase(word.charAt(word.length()-1))){
                    word+=ch;
                    continue;
                }else if(!word.trim().equals("")){
                    words.add(word);
                    word = String.valueOf(ch);
                    continue;
                }

            }
            if(i==0&&Character.isLowerCase(ch)){
                word = String.valueOf(ch).toUpperCase(Locale.ROOT);
                continue;
            }
            word += String.valueOf(ch);
        }
        words.add(word);
        String label = "";
        for(int i=0; i<words.size(); i++){
            label += words.get(i);
            if(i<words.size()-1){
                label += " ";
            }
        }
        return label;

    }

    public JSONObject getExtra() {

        return extra;
    }

    public void setExtra(JSONObject extra) {
        this.extra = extra;
    }

    public Datum with(JSONObject extra){
        setExtra(extra);
        return this;
    }
}
