package com.sinxel.lairda.models;

public class AppNotification {
    public enum Type{
        ERROR,
        WARNING,
        INFO,
        SUCCESS
    }
    Type type;
    String title;
    String message;
    public AppNotification(){

    }
    public AppNotification(Type type, String title, String message){
        this.type = type;
        this.title = title;
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
