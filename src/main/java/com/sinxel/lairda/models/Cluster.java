package com.sinxel.lairda.models;

public class Cluster {
    private int id = -1;
    private String path;
    private long epoch;
    private int children = 0;
    private DataMap data = new DataMap();
    public Cluster(){

    }
    public Cluster(int id, String path, String title){
        this.id = id; this.path = path;
        data.put("title", new Datum("title", title));

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getEpoch() {
        return epoch;
    }

    public void setEpoch(long epoch) {
        this.epoch = epoch;
    }

    public int getChildren() {
        return children;
    }

    public void setChildren(int children) {
        this.children = children;
    }

    public DataMap getData() {
        return data;
    }

    public void setData(DataMap data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Cluster{" +
                "id=" + id +
                ", path='" + path + '\'' +
                ", epoch=" + epoch +
                ", children=" + children +
                ", data=" + data +
                '}';
    }
}
