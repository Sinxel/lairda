package com.sinxel.lairda.dialogs;

import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.models.Datum;
import com.sinxel.lairda.models.DataMap;
import com.sinxel.lairda.values.Dimensions;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class DBConnectionDialog extends Dialog<DataMap> {
    DataMap result;
    public DBConnectionDialog(){
        setTitle("New Connection");
        loadIcon();
        loadTheme();
        buildDialog();
    }

    void buildDialog(){
        GridPane grid = new GridPane();
        grid.setVgap(Dimensions.SMALL_GAP);
        grid.setHgap(Dimensions.SMALL_GAP);
        grid.setPadding(new Insets(Dimensions.SAFE_SPACE));

        Label lblConnectionName  = new Label("Connection Name");
        TextField txtConnectionName = new TextField();

        grid.add(lblConnectionName, 0, 0);
        grid.add(txtConnectionName, 1, 0, 3, 1);

        Label lblDatabaseType = new Label("Database Type");
        grid.add(lblDatabaseType, 0, 1);
        ComboBox<String> cmbDatabaseType = new ComboBox<>();
        cmbDatabaseType.getItems().add("PostgreSQL");
        grid.add(cmbDatabaseType, 1, 1, 3, 1);

        Label lblServer = new Label("Server");
        TextField txtHost = new TextField("localhost");
        grid.add(lblServer, 0, 2);
        grid.add(txtHost, 1, 2);

        Label lblPort = new Label("Port");
        TextField txtPort = new TextField("5432");

        grid.add(lblPort, 2, 2);
        grid.add(txtPort, 3, 2);

        Label lblUser = new Label("Username");
        Label lblPassword = new Label("Password");

        TextField txtUser = new TextField();
        PasswordField txtPassword = new PasswordField();

        grid.add(lblUser, 0, 3);
        grid.add(txtUser, 1, 3);
        grid.add(lblPassword, 2, 3);
        grid.add(txtPassword, 3, 3);


        getDialogPane().setContent(grid);

        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        final Button btnOk = (Button) getDialogPane().lookupButton(ButtonType.OK);
        btnOk.addEventFilter(ActionEvent.ACTION, ev->{
            System.out.println(cmbDatabaseType.getSelectionModel().getSelectedItem());
            boolean canContinue = true;
            String connectionName = txtConnectionName.getText().trim();
            String databaseType = cmbDatabaseType.getSelectionModel().getSelectedItem();
            String host = txtHost.getText().trim();
            String port = txtPort.getText().trim();
            if(connectionName.isEmpty()) canContinue = false;
            if(databaseType==null) canContinue = false;
            if(host.isEmpty()) canContinue = false;
            if(port.isEmpty()) canContinue = false;
            if(!canContinue) ev.consume();
            //if(item==null) ev.consume();
        });
        setResultConverter((btn)->{
            result = new DataMap();
            String connectionName = txtConnectionName.getText().trim();
            String databaseType = cmbDatabaseType.getSelectionModel().getSelectedItem();
            String host = txtHost.getText().trim();
            String port = txtPort.getText().trim();
            String user = txtUser.getText().trim();
            String password = txtPassword.getText().trim();
            result.add(new Datum("title", connectionName));
            result.add(new Datum("type", databaseType));
            result.add(new Datum("host", host));
            result.add(new Datum(Datum.Type.Number, "port", port));
            result.add(new Datum("user", user));
            result.add(new Datum(Datum.Type.Secret, "password", password));
            if(btn.equals(ButtonType.CANCEL)) return null;
            return  result;
        });
    }

    void loadIcon(){
        Stage stage = (Stage) getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("file:assets/images/icon.png"));
    }
    void loadTheme(){
        getDialogPane().getStylesheets().add(Lairda.getInstance().getTheme());
    }
}
