package com.sinxel.lairda.dialogs;

import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.factories.ClusterTreeCell;
import com.sinxel.lairda.models.Cluster;
import com.sinxel.lairda.models.Datum;
import com.sinxel.lairda.models.DataMap;
import com.sinxel.lairda.services.ClusterManager;
import com.sinxel.lairda.values.Dimensions;
import com.sinxel.lairda.values.ICIcons;
import com.sinxel.lairda.widgets.IcoFont;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.CustomTextField;

import java.util.ArrayList;
import java.util.Optional;

public class ClusterPicker extends Dialog<Integer> {
    TreeView<Cluster> tvwClusters;
    CustomTextField txtSearch;
    ArrayList<Integer> loadedItems;
    String prompt;
    public ClusterPicker(){

        init();
    }

    public ClusterPicker(String prompt){
        this.prompt = prompt;
        init();
    }

    void init(){
        loadedItems = new ArrayList<>();
        setTitle("Select Cluster");
        setWidth(Dimensions.DEFAULT_DIALOG_WIDTH);
        setHeight(Dimensions.DEFAULT_DIALOG_HEIGHT);
        setResizable(true);
        loadIcon();
        loadTheme();
        getDialogPane().setContent(buildDialog());
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        final Button btnOk = (Button) getDialogPane().lookupButton(ButtonType.OK);
        btnOk.addEventFilter(ActionEvent.ACTION, ev->{
            TreeItem<Cluster> item = tvwClusters.getSelectionModel().getSelectedItem();
            if(item==null) ev.consume();
        });
        setResultConverter((btn)->{
            if(btn.equals(ButtonType.CANCEL)) return null;
            TreeItem<Cluster> item = tvwClusters.getSelectionModel().getSelectedItem();

            return  item.getValue().getId();
        });
    }

    VBox buildDialog(){
        VBox container = new VBox();
        container.setPadding(new Insets(Dimensions.SAFE_SPACE));
        txtSearch = new CustomTextField();
        txtSearch.setOnKeyPressed(ev->{
            if(ev.getCode()== KeyCode.ENTER){
                searchCluster();
                ev.consume();
            }
        });
        txtSearch.setPromptText("Search Clusters");
        txtSearch.setRight(new IcoFont(ICIcons.SEARCH_1));
        container.setSpacing(Dimensions.SMALL_GAP);
        tvwClusters = new TreeView<>();
        tvwClusters.setMinWidth(Dimensions.DEFAULT_DIALOG_WIDTH);
        VBox.setVgrow(tvwClusters, Priority.ALWAYS);
        container.getChildren().add(txtSearch);
        if(prompt!=null){
            Label lblPrompt = new Label(prompt);
            container.getChildren().add(lblPrompt);
        }
        buildTree();
        container.getChildren().add(tvwClusters);
        return container;
    }
    void buildTree(){
        tvwClusters.setCellFactory(param->new ClusterTreeCell());
        ContextMenu cxmOptions = new ContextMenu();
        MenuItem mniAddCluster = new MenuItem("New Cluster");
        mniAddCluster.setGraphic(new IcoFont(ICIcons.PLUS_CIRCLE));
        cxmOptions.getItems().add(mniAddCluster);
        mniAddCluster.setOnAction(ev->createCluster());
        populateTree();
    }

    void populateTree(){
        tvwClusters.setRoot(null);
        loadedItems = new ArrayList<>();
        TreeItem<Cluster> root = new TreeItem<>(new Cluster(0, "0/", "[ROOT]"));
        tvwClusters.setRoot(root);
        root.expandedProperty().addListener(((observable, oldValue, newValue) -> {
            if(newValue) populateChildrenOf(root);
        }));
        ArrayList<Cluster> clusters = ClusterManager.getAllClusters(0);
        clusters.forEach(cluster->{
            TreeItem<Cluster> item = new TreeItem<>(cluster);
            loadedItems.add(cluster.getId());
            item.expandedProperty().addListener(((observable, oldValue, newValue) -> {
                if(newValue) populateChildrenOf(item);
            }));
            root.getChildren().add(item);
        });

    }
    void populateChildrenOf(TreeItem<Cluster> parent){
        parent.getChildren().forEach(child->{
            ArrayList<Cluster> children = ClusterManager.getAllClusters(child.getValue().getId());
            children.forEach(cluster -> {
                if(loadedItems.contains(cluster.getId())) return;
                loadedItems.add(cluster.getId());
                TreeItem<Cluster> item = new TreeItem<>(cluster);
                child.getChildren().add(item);
                item.expandedProperty().addListener((observable, oldValue, newValue) -> {
                    if(newValue) populateChildrenOf(item);
                });
            });
        });
    }
    void createCluster(){
        TreeItem<Cluster> item = tvwClusters.getSelectionModel().getSelectedItem();
        if(item==null) return;
        TextInputDialog clusterNameDialog = new TextInputDialog("Cluster Name");
        clusterNameDialog.setTitle("New Cluster");
        clusterNameDialog.setHeaderText("Type new cluster's name");
        Optional<String> result = clusterNameDialog.showAndWait();
        result.ifPresent(name->{
            DataMap data = new DataMap();
            data.add(new Datum("title", name));
            int id = ClusterManager.insertCluster(item.getValue().getId(), data);
            Cluster cluster= ClusterManager.getCluster(id);
            if(cluster!=null) loadedItems.add(cluster.getId());
            TreeItem<Cluster> clusterItem = new TreeItem<>();
            item.setExpanded(true);
            item.getChildren().add(clusterItem);
            tvwClusters.getSelectionModel().select(clusterItem);

        });

    }
    void searchCluster(){

    }
    void loadIcon(){
        Stage stage = (Stage) getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("file:assets/images/icon.png"));
    }
    void loadTheme(){
        getDialogPane().getStylesheets().add(Lairda.getInstance().getTheme());
    }


}
