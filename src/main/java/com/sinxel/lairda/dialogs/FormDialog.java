package com.sinxel.lairda.dialogs;

import com.sinxel.lairda.Lairda;
import com.sinxel.lairda.models.DataMap;
import com.sinxel.lairda.models.Datum;
import com.sinxel.lairda.values.Dimensions;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.HashMap;

public class FormDialog extends Dialog<DataMap> {
    DataMap data;
    HashMap<String, Node> controls;
    Label lblStatus;
    public FormDialog(String title, DataMap data){
        setTitle(title);
        setResizable(true);
        setWidth(Dimensions.DEFAULT_DIALOG_WIDTH);
        setHeight(Dimensions.DEFAULT_DIALOG_HEIGHT);
        loadIcon();
        loadTheme();
        this.data = data;
        buildDialog();
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        final Button btnOk = (Button) getDialogPane().lookupButton(ButtonType.OK);
        btnOk.addEventFilter(ActionEvent.ACTION, ev->{

            ArrayList<String> errorKeys = new ArrayList<>();
            data.forEach((key, datum)->{
                if(!validateField(key, datum)){
                    controls.get(key).getStyleClass().add("error");
                    errorKeys.add(key);
                }
            });
            if(errorKeys.size()>0) ev.consume();
            lblStatus.setText("Please make sure the fields marked in red are valid");
            //show errors
            //if(item==null) ev.consume();
        });
        setResultConverter((btn)->{

            data.forEach((key, datum)->{
                datum.setValue(getControlValue(key, datum));
            });
            if(btn.equals(ButtonType.CANCEL)) return null;
            return  data;
        });

    }
    boolean validateField(String key, Datum datum){
        String value = getControlValue(key, datum);
        boolean isRequired = datum.getExtra()!=null&&datum.getExtra().has("isRequired")&&datum.getExtra().getBoolean("isRequired");
        if(!isRequired) return true;
        return !value.trim().equals("");
    }
    String getControlValue(String key, Datum datum){
        Node control = controls.get(key);
        switch (datum.getType()){
            case Secret -> {
                return ((PasswordField) control).getText();
            }
            default -> {
                return ((TextField) control).getText();
            }
        }
    }
    void buildDialog(){
        ScrollPane scroller = new ScrollPane();

        controls = new HashMap<>();
        GridPane grid = new GridPane();
        grid.getStyleClass().add("form-grid");
        grid.setHgap(Dimensions.SMALL_GAP);
        grid.setVgap(Dimensions.SMALL_GAP);
        grid.setPadding(new Insets(Dimensions.SAFE_SPACE));
        data.forEach((key, datum)->{
            Label lblKey = new Label(datum.getKeyLabel());
            Node control = getControlForType(datum.getType(), datum.getValue());
            controls.put(key, control);
            grid.add(lblKey, 0, grid.getRowCount() );
            grid.add(control, 1, grid.getRowCount()-1 );
        });
        lblStatus = new Label();
        lblStatus.getStyleClass().add("error");
        grid.add(lblStatus, 0, grid.getRowCount(), 2, 1);
        scroller.setContent(grid);
        getDialogPane().setContent(scroller);
    }

    Node getControlForType(Datum.Type type, String value){
        switch (type){
            case Secret -> {
                PasswordField txtPassword = new  PasswordField();
                txtPassword.setMinWidth(240);
                return txtPassword;
            }
            case ReadOnly -> {
                TextField txtControl = new TextField(value==null?"":value);
                txtControl.setEditable(false);
                return  txtControl;
            }
            default ->{
                TextField txtControl =  new TextField(value==null?"":value);
                txtControl.setMinWidth(240);
                return txtControl;
            }
        }
    }

    void loadIcon(){
        Stage stage = (Stage) getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("file:assets/images/icon.png"));
    }
    void loadTheme(){
        getDialogPane().getStylesheets().add(Lairda.getInstance().getTheme());
    }

}
