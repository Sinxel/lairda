package com.sinxel.lairda.values;

public class Dimensions {
    public static final double SAFE_SPACE = 20.0;
    public static final double SIDE_MENU = 72.0;
    public static final double LOGO_SIZE = 48.0;
    public static final double SIDE_MENU_SPACING = 10.0;
    public static final double ICON_BUTTON_SIZE = 24.0;
    public static final double SMALL_GAP = 10.0;
    public static final double DEFAULT_DIALOG_WIDTH = 540;
    public static final double DEFAULT_DIALOG_HEIGHT = 400;
}
