package com.sinxel.lairda.widgets;

import javafx.scene.layout.Region;

public class VSpace extends Region {
    public VSpace(double height){
        setHeight(height);
    }
}
