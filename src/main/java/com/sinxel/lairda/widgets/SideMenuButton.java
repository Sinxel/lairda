package com.sinxel.lairda.widgets;

import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

public class SideMenuButton extends Label {
    String icon;
    String label;
    public SideMenuButton(String icon, String label){
        getStyleClass().clear();
        getStyleClass().add("md-icon");
        setTooltip(new Tooltip(label));
        setText(icon);
    }



}
