package com.sinxel.lairda.widgets;

import javafx.scene.control.Label;

public class MdIcon extends Label {
    protected String code;
    protected double size = 16;
    protected String color = "#FFFFFF";

    public MdIcon(String code){
        getStyleClass().clear();
        getStyleClass().add("md-icon");
        setCode(code);
    }

    public MdIcon(String code, double size){
        setCode(code);
        setSize(size);
    }

    public MdIcon(String code, double size, String color){
        setCode(code);
        setSize(size);
        setColor(color);
    }

    void setCode(String code){
        this.code = code;
        updateStyle();
    }

    void setSize(double size){
        this.size = size;
        updateStyle();
    }

    /**
     *
     * Changes the color of the icon
     * @param color String representing the color, the string can be the name of the color (e.g. red) or a hexadecimal value prefixed with '#' (e.g. #FF0000)
     */
    void setColor(String color){
        this.color = color;
        updateStyle();
    }
    void updateStyle(){
        setText(code);
        setStyle("-fx-font-family: \'Material Icons\'; -fx-font-size: "+size+"; -fx-text-fill: "+color+"; -fx-background-color: transparent");
    }

}
