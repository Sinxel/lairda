package com.sinxel.lairda.widgets;

import javafx.scene.control.Label;

public class TitleLabel extends Label {
    public TitleLabel(String label){
        getStyleClass().add("title");
        setText(label);
    }
}
