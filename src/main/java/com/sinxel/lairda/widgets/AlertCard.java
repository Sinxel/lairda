package com.sinxel.lairda.widgets;

import com.sinxel.lairda.models.AppNotification;
import com.sinxel.lairda.values.ICIcons;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class AlertCard extends HBox {
    public AlertCard(AppNotification notification){
        setAlignment(Pos.CENTER_LEFT);
        String className = "alert";
        String iconCode = ICIcons.EXCLAMATION;
        switch (notification.getType()){
            case ERROR ->{
                className = "error";
                iconCode = ICIcons.ERROR;
            }
            case INFO -> {
                className = "info";
                iconCode = ICIcons.INFO;
            }
            case WARNING -> {
                className = "warning";
                iconCode = ICIcons.WARNING;
            }
            case SUCCESS -> {
                className = "success";
                iconCode = ICIcons.CHECK;
            }
        }
        getStyleClass().addAll("alert", className);
        IcoFont icon = new IcoFont(iconCode);
        icon.setMinWidth(48);
        HBox.setHgrow(icon, Priority.ALWAYS);
        getChildren().add(icon);

        VBox alertContent = new VBox();
        alertContent.setSpacing(5);
        Label lblTitle = new Label(notification.getTitle());
        lblTitle.getStyleClass().add("title");
        alertContent.getChildren().add(lblTitle);
        Label lblMessage = new Label(notification.getMessage());
        lblMessage.setWrapText(true);

        alertContent.getChildren().add(lblMessage);
        getChildren().add(alertContent);
    }
}
