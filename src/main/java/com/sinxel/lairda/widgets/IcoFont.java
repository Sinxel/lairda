package com.sinxel.lairda.widgets;


import javafx.scene.control.Label;

public class IcoFont extends Label {
    protected String code;


    public IcoFont(String code){
        getStyleClass().clear();
        getStyleClass().add("ico-font");
        setText(code);
    }


}
