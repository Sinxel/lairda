package com.sinxel.lairda.widgets;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;

public class CircleButton extends Button {

    public CircleButton(String icon){



        getStyleClass().add("circle-button");
        setText(icon);
    }
}
