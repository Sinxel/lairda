package com.sinxel.lairda.widgets;

import com.sinxel.lairda.providers.Postgres;
import com.sinxel.lairda.values.MdIcons;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class WidgetBox extends VBox {
    public WidgetBox(){
        setMinSize(240, 160);
        setMaxSize(240, 160);
        setAlignment(Pos.CENTER);
        getStyleClass().add("widget-box");
        MdIcon btnAdd = new  MdIcon(MdIcons.ADD_CIRCLE, 56, "#19a5a6");

        getChildren().add(btnAdd);
    }
}
