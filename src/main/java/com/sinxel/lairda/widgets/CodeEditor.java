package com.sinxel.lairda.widgets;

import com.sinxel.lairda.highlighters.JSONHighlighter;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.model.Paragraph;

import java.util.Collection;


public class CodeEditor extends StackPane {
    public enum Syntax{
        None,
        JSON,
        XML,
        SQL
    }
    CodeArea codeArea;
    Syntax syntax = Syntax.None;

    public CodeEditor(){
        init();
    }

    public CodeEditor(Syntax syntax){
        this.syntax = syntax;
        init();
    }
    public CodeArea getCodeArea(){
        return codeArea;
    }

    public void setSyntax(Syntax syntax) {
        this.syntax = syntax;
    }

    public void setText(String text){
        codeArea.replaceText(text);

    }
    public String getText(){
        return codeArea.getText();
    }

    void init(){
        codeArea = new CodeArea();
        codeArea.getStyleClass().add("code");
        codeArea.setParagraphGraphicFactory(LineNumberFactory.get(codeArea));

        codeArea.textProperty().addListener(((observable, oldValue, newValue) -> {
            if(syntax==Syntax.JSON){
                JSONHighlighter highlighter = new JSONHighlighter(newValue);
                codeArea.setStyleSpans(0, highlighter.highlight());
            }
        }));


        codeArea.setWrapText(true);
        codeArea.addEventFilter(KeyEvent.KEY_PRESSED, ev->{
            if(ev.getCode()== KeyCode.ENTER){
                ev.consume();
                indentNextLine();
            }
        });
        codeArea.setLineHighlighterOn(true);

        VirtualizedScrollPane<CodeArea> sp = new VirtualizedScrollPane<>(codeArea);
        getChildren().add(sp);
    }

    void indentNextLine(){
        int paragraphIndex= codeArea.getCurrentParagraph();
        Paragraph<Collection<String>, String, Collection<String>> par = codeArea.getParagraph(paragraphIndex);

        String line = par.getText();
        String pre = "";
        for(int i=0; i<line.length(); i++){
            char c = line.charAt(i);
            if(c==' '||c=='\t'){
                pre += c;
            }else{
                break;
            }
        }
        char last = line.charAt(line.length()-1);
        if(last=='{'||last=='[') pre += '\t';
        codeArea.insert(codeArea.getCaretPosition(), "\n"+pre, "");

    }
}
