package com.sinxel.lairda.widgets;

import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.StackPane;

public class FullProgressIndicator extends StackPane {
    public FullProgressIndicator(){
        getChildren().add(new ProgressIndicator());
    }
}
