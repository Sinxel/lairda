package com.sinxel.lairda.widgets;

public class IconButton extends MdIcon{

    public IconButton(String code) {
        super(code);
    }

    public IconButton(String code, double size) {
        super(code, size);
    }

    public IconButton(String code, double size, String color) {
        super(code, size, color);
    }

    @Override
    void updateStyle() {
        setText(code);
        setStyle("-fx-font-family: \'Material Icons\'; -fx-font-size: "+size+"; -fx-text-fill: "+color+"; -fx-background-color: transparent; -fx-cursor: hand;");
    }
}
